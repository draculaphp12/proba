# back
# устанавливаем самую лёгкую версию JVM
FROM openjdk:8-jdk-alpine

# указываем точку монтирования для внешних данных внутри контейнера (как мы помним, это Линукс)
VOLUME /tmp

# внешний порт, по которому наше приложение будет доступно извне
EXPOSE 8080

# указываем, где в нашем приложении лежит джарник
ARG JAR_FILE=build/libs/proba-0.0.1-SNAPSHOT.jar

# добавляем джарник в образ под именем proba-0.0.1-SNAPSHOT.jar
ADD ${JAR_FILE} proba-0.0.1-SNAPSHOT.jar

# команда запуска джарника
ENTRYPOINT ["java","-jar","/proba-0.0.1-SNAPSHOT.jar"]