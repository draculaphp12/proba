package ru.proba.postgres.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.proba.postgres.models.company.Company;
import ru.proba.postgres.models.company.Supervisor;
import ru.proba.postgres.models.internship.Internship;
import ru.proba.postgres.repositories.SupervisorRepository;
import java.util.List;

@Component
public class SupervisorService {

    @Autowired
    private SupervisorRepository supervisorRepository;

    @Autowired
    private CompanyService companyService;

    public Supervisor findSupervisorById(Long supervisorId) {
        return supervisorRepository.findById(supervisorId).orElse(null);
    }

    public Supervisor saveSupervisor(Supervisor supervisor) {
        Company company = companyService.getCurrentCompany();
        if (company != null) {
            supervisor.setCompany(company);
            return supervisorRepository.save(supervisor);
        }
        return null;
    }

    @Transactional
    public List<Internship> findInternships(Long supervisorId) {
        return supervisorRepository.findInternships(supervisorId);
    }

    @Transactional
    public boolean deleteSupervisor(Long supervisorId) {
        Supervisor supervisor = supervisorRepository.findById(supervisorId).orElse(null);
        if (supervisor != null) {
            supervisorRepository.deleteInternships(supervisorId);
            supervisorRepository.customDelete(supervisorId);
            return true;
        }
        return false;
    }
}
