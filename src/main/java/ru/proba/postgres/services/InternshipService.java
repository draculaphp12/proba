package ru.proba.postgres.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.proba.postgres.models.company.Company;
import ru.proba.postgres.models.company.Supervisor;
import ru.proba.postgres.models.internship.Description;
import ru.proba.postgres.models.internship.Internship;
import ru.proba.postgres.models.internship.Requirement;
import ru.proba.postgres.repositories.SupervisorRepository;
import ru.proba.postgres.repositories.internship.DescriptionRepository;
import ru.proba.postgres.repositories.internship.InternshipRepository;
import ru.proba.postgres.repositories.internship.RequirementRepository;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class InternshipService {
    @Autowired
    private InternshipRepository internshipRepository;

    @Autowired
    private SupervisorRepository supervisorRepository;

    @Autowired
    private DescriptionRepository descriptionRepository;

    @Autowired
    private RequirementRepository requirementRepository;

    @Autowired
    private CompanyService companyService;

    public void deleteInternshipRequirement(Long internshipId) {
        Requirement requirement = requirementRepository.findRequirementByInternship_Id(internshipId);
        if (requirement != null) {
            requirementRepository.deleteByInternshipId(internshipId);
        }
    }

    public void deleteInternshipDescription(Long internshipId) {
        Description description = descriptionRepository.findDescriptionByInternship_Id(internshipId);
        if (description != null) {
            descriptionRepository.deleteByInternshipId(internshipId);
        }
    }

    public List<Internship> findAll() {
        return internshipRepository.findAll();
    }

    public Internship findInternshipById(Long internshipId) {
        return findAll()
                .stream()
                .filter(i -> Objects.equals(i.getId(), internshipId))
                .collect(Collectors.toList()).get(0);
    }

    public void saveInternship(Internship internship, Requirement requirements, Description description) {
        Company company = companyService.getCurrentCompany();
        if (company != null) {
            internship.setCompany(company);
            if (internship.getSupervisor() != null) {
                Supervisor supervisor = supervisorRepository.findById(internship.getSupervisor().getId()).orElse(null);
                internship.setSupervisor(supervisor);
            }
            Internship createdInternship = internshipRepository.save(internship);
            requirements.setId(createdInternship.getId());
            requirements.setInternship(createdInternship);
            description.setId(createdInternship.getId());
            description.setInternship(createdInternship);
            requirementRepository.save(requirements);
            descriptionRepository.save(description);
        }
    }

    @Transactional
    public void deleteInternship(Long internshipId) {
        Internship internship = findInternshipById(internshipId);
        if (internship != null) {
            deleteInternshipRequirement(internshipId);
            deleteInternshipDescription(internshipId);
            internshipRepository.customDelete(internshipId);
        }
    }
}
