package ru.proba.postgres.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import ru.proba.postgres.models.Role;
import ru.proba.postgres.models.User;
import ru.proba.postgres.models.company.Company;
import ru.proba.postgres.repositories.CompanyJPARepository;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class CompanyService {

    @Autowired
    private CompanyJPARepository companyJPARepository;

    @Autowired
    private UserService userService;

    public List<Company> allCompanies() {
        return companyJPARepository.findAll();
    }

    public Company findCompanyById(Long companyId) {
        Optional<Company> companyFromDb = companyJPARepository.findById(companyId);
        return companyFromDb.orElse(null);
    }

    public Company findCompanyByName(String name) {
        return companyJPARepository.findCompanyByName(name);
    }

    public Company addCompany(Company company, User user) {
        User savedUser = userService.saveUser(user, new Role(2L, "ROLE_COMPANY"));
        Company foundedCompany = findCompanyByName(company.getName());
        if (foundedCompany != null || savedUser == null) {
            return null;
        } else {
            company.setUser(savedUser);
            return companyJPARepository.save(company);
        }
    }

    public boolean updateCompany(Long companyId, Company company) {
        Company foundedCompany = findCompanyById(companyId);
        if (foundedCompany == null) {
            return false;
        }
        if (!Objects.equals(foundedCompany.getName(), company.getName())) {
            boolean isNewCompanyNameEngaged = findCompanyByName(company.getName()) != null;
            if (isNewCompanyNameEngaged) {
                return false;
            }
        }
        foundedCompany.setName(company.getName());
        foundedCompany.setDescription(company.getDescription());
        foundedCompany.setCountWorkers(company.getCountWorkers());
        foundedCompany.setType(company.getType());
        foundedCompany.setSiteUrl(company.getSiteUrl());
        companyJPARepository.save(foundedCompany);
        return true;
    }

    public boolean deleteCompany(Long companyId) {
        if (companyJPARepository.findById(companyId).isPresent()) {
            companyJPARepository.deleteById(companyId);
            return true;
        }
        return false;
    }

    public Company getCurrentCompany() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = (User) authentication.getPrincipal();
        return companyJPARepository.findCompanyByUser(user);
    }
}
