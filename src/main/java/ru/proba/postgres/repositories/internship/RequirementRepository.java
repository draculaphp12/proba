package ru.proba.postgres.repositories.internship;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import ru.proba.postgres.models.internship.Requirement;

public interface RequirementRepository extends JpaRepository<Requirement, Long> {
    Requirement findRequirementByInternship_Id(Long internshipId);

    @Modifying
    @Query("delete from Requirement i where i.id = ?1")
    void deleteByInternshipId(Long entityId);
}
