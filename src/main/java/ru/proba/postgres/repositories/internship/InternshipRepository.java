package ru.proba.postgres.repositories.internship;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import ru.proba.postgres.models.internship.Internship;

import java.util.Optional;

public interface InternshipRepository extends JpaRepository<Internship, Long> {
    @Modifying
    @Query("delete from Internship i where i.id = ?1")
    void customDelete(Long entityId);
}
