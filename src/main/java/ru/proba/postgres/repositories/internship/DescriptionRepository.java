package ru.proba.postgres.repositories.internship;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import ru.proba.postgres.models.internship.Description;

public interface DescriptionRepository extends JpaRepository<Description, Long> {
    Description findDescriptionByInternship_Id(Long internshipId);

    @Modifying
    @Query("delete from Description i where i.id = ?1")
    void deleteByInternshipId(Long entityId);
}
