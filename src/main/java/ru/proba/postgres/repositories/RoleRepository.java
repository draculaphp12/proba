package ru.proba.postgres.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.proba.postgres.models.Role;

public interface RoleRepository extends JpaRepository<Role, Long> {
}
