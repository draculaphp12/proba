package ru.proba.postgres.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import ru.proba.postgres.models.User;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);

    @Query("select i from User i where i.id = ?1")
    User findUserById(Long entityId);

    @Modifying
    @Query("delete from User i where i.id = ?1")
    void deleteUser(Long entityId);

    @Modifying
    @Query("delete from Company i where i.user.id = ?1")
    void deleteCompanyUser(Long entityId);
}
