package ru.proba.postgres.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import ru.proba.postgres.models.company.Supervisor;
import ru.proba.postgres.models.internship.Internship;
import java.util.List;

public interface SupervisorRepository extends JpaRepository<Supervisor, Long> {
    @Modifying
    @Query("delete from Supervisor s where s.id = ?1")
    void customDelete(Long entityId);

    @Query("SELECT i FROM Internship i WHERE i.supervisor.id=?1")
    List<Internship> findInternships(Long entityId);

    @Modifying
    @Query("delete from Internship i where i.supervisor.id = ?1")
    void deleteInternships(Long entityId);
}
