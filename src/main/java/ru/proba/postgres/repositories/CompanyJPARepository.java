package ru.proba.postgres.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.proba.postgres.models.User;
import ru.proba.postgres.models.company.Company;

public interface CompanyJPARepository extends JpaRepository<Company, Long> {
    Company findCompanyByName(String name);

    Company findCompanyByUser(User user);
}
