package ru.proba.postgres.models.company;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import ru.proba.postgres.models.User;
import ru.proba.postgres.models.internship.Internship;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "companies")
public class Company {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    @Setter
    private Long id;

    @Getter
    @Setter
    @Column(unique = true, nullable = false)
    private String name;

    @Getter
    @Setter
    @Column(nullable = false)
    private String type;

    @Getter
    @Setter
    @Column(nullable = false)
    private int countWorkers;

    @Getter
    @Setter
    @Column(columnDefinition="TEXT", nullable = false)
    private String description;

    @Getter
    @Setter
    private String siteUrl;

    @Getter
    @Setter
    @OneToMany(mappedBy = "company", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    private List<Internship> internships = new ArrayList<>();

    @Getter
    @Setter
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(mappedBy = "company", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Supervisor> supervisors = new ArrayList<>();

    @Getter
    @Setter
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "user_id")
    private User user;
}
