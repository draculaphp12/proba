package ru.proba.postgres.models.company;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.proba.postgres.models.internship.Internship;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "company_supervisors")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Supervisor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String email;

    @Column(nullable = false)
    private String position;

    @Column(nullable = false)
    private String phone;

    @ManyToOne(optional = false)
    @JoinColumn(name = "company_id")
    private Company company;

    @OneToMany(mappedBy = "supervisor", fetch = FetchType.EAGER)
    private List<Internship> internships = new ArrayList<>();
}
