package ru.proba.postgres.models.internship;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.proba.postgres.models.company.Company;
import ru.proba.postgres.models.company.Supervisor;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "internships")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Internship {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String title;

    @Column(nullable = false)
    private Date startDate;

    @Column(nullable = false)
    private Date endDate;

    @Column(nullable = false)
    private String city;

    @Column(nullable = false)
    private String address;

    @ManyToOne(optional = false)
    @JoinColumn(name = "company_id")
    private Company company;

    @OneToOne(optional = false, mappedBy = "internship", cascade = CascadeType.ALL)
    private Requirement requirements;

    @OneToOne(optional = false, mappedBy = "internship", cascade = CascadeType.ALL)
    private Description description;

    @ManyToOne
    @JoinColumn(name = "supervisor_id")
    private Supervisor supervisor;
}
