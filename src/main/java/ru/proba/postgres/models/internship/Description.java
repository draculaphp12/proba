package ru.proba.postgres.models.internship;

import lombok.Getter;
import lombok.Setter;
import javax.persistence.*;


@Entity
@Table(name = "internship_descriptions")
@Getter
@Setter
public class Description {
    @Id
    @Column(name = "internship_id")
    private Long id;

    private Boolean jobGuarantee = false;

    private Double salary = 0.0;

    private String workSchedule;

    @Column(columnDefinition="TEXT")
    private String requiredSkills;

    @Column(columnDefinition="TEXT")
    private String tasksDescription;

    @Column(columnDefinition="TEXT")
    private String conditions;

    @OneToOne(optional = false, cascade = CascadeType.ALL)
    @JoinColumn(name = "internship_id")
    private Internship internship;
}
