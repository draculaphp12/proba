package ru.proba.postgres.models.internship;

import lombok.Getter;
import lombok.Setter;
import javax.persistence.*;

@Entity
@Table(name = "internship_requirements")
@Getter
@Setter
public class Requirement {
    @Id
    @Column(name = "internship_id")
    private Long id;

    @Column(nullable = false)
    private String educationLevel;

    @Column(nullable = false)
    private String course;

    @Column(nullable = false)
    private String industry;

    @OneToOne(optional = false, cascade = CascadeType.ALL)
    @JoinColumn(name = "internship_id")
    private Internship internship;
}
