package ru.proba.controllers.employers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;
import ru.proba.postgres.models.company.Company;
import ru.proba.postgres.models.company.Supervisor;
import ru.proba.postgres.models.internship.Description;
import ru.proba.postgres.models.internship.Internship;
import ru.proba.postgres.models.internship.Requirement;
import ru.proba.postgres.services.CompanyService;
import ru.proba.postgres.services.InternshipService;

import java.util.List;

@Controller
@RequestMapping(value = "/employers/internships")
public class InternshipsController {

    @Autowired
    private CompanyService companyService;

    @Autowired
    private InternshipService internshipService;

    @GetMapping
    public String internshipsList(Model model) {
        Company company = companyService.getCurrentCompany();
        if (company != null) {
            model.addAttribute("internships", company.getInternships());
        } else {
            model.addAttribute("internships", null);
        }
        return "employers/internships/internships_list";
    }

    @GetMapping(value = "/{internshipId}")
    @PreAuthorize("@internshipService.findInternshipById(#internshipId).company.user.username == @companyService.currentCompany.user.username")
    public String internship(@PathVariable("internshipId") Long internshipId, Model model) {
        model.addAttribute("internship", internshipService.findInternshipById(internshipId));
        return "employers/internships/internship_detailed";
    }

    @GetMapping(value = "/add")
    public String create(Model model) {
        Company company = companyService.getCurrentCompany();
        if (company != null) {
            List<Supervisor> supervisorsList = company.getSupervisors();
            model.addAttribute("supervisorsList", supervisorsList);
        } else {
            model.addAttribute("supervisorsList", null);
        }
        model.addAttribute("internship", new Internship());
        model.addAttribute("requirement", new Requirement());
        model.addAttribute("description", new Description());
        return "employers/internships/internship_create";
    }

    @PostMapping(value = "/add")
    public RedirectView add(
        @ModelAttribute("internship") Internship internship,
        @ModelAttribute("requirement") Requirement requirements,
        @ModelAttribute("description") Description description
    ) {
        internshipService.saveInternship(internship, requirements, description);
        return new RedirectView("/employers/internships");
    }

    @PostMapping("/{internshipId}/delete")
    @PreAuthorize("@internshipService.findInternshipById(#internshipId).company.user.username == @companyService.currentCompany.user.username")
    public RedirectView delete(@PathVariable("internshipId") Long internshipId) {
        internshipService.deleteInternship(internshipId);
        return new RedirectView("/employers/internships");
    }

    @GetMapping(value = "/{internshipId}/update")
    @PreAuthorize("@internshipService.findInternshipById(#internshipId).company.user.username == @companyService.currentCompany.user.username")
    public String update(Model model, @PathVariable Long internshipId) {
        Internship internship = internshipService.findInternshipById(internshipId);
        List<Supervisor> supervisorsList = companyService.getCurrentCompany().getSupervisors();
        model.addAttribute("supervisorsList", supervisorsList);
        model.addAttribute("internship", internship);
        model.addAttribute("requirement", internship.getRequirements());
        model.addAttribute("description", internship.getDescription());
        return "employers/internships/internship_update";
    }

    @PostMapping("/{internshipId}/update")
    @PreAuthorize("@internshipService.findInternshipById(#internshipId).company.user.username == @companyService.currentCompany.user.username")
    public RedirectView save(
        @PathVariable Long internshipId,
        @ModelAttribute("internship") Internship internship,
        @ModelAttribute("requirement") Requirement requirements,
        @ModelAttribute("description") Description description
    ) {
        internshipService.saveInternship(internship, requirements, description);
        return new RedirectView("/employers/internships");
    }
}
