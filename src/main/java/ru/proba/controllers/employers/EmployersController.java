package ru.proba.controllers.employers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.view.RedirectView;
import ru.proba.postgres.models.User;
import ru.proba.postgres.models.company.Company;
import ru.proba.postgres.services.CompanyService;
import ru.proba.postgres.services.UserService;

@Controller
@RequestMapping(value = "/employers")
public class EmployersController {
    @Autowired
    private CompanyService companyService;

    @Autowired
    private UserService userService;

    @GetMapping(value = "/home")
    public String home(Model model) {
        return "employers/employers_home";
    }

    @GetMapping(value = "/company")
    public String companyInfo(Model model) {
        Company company = companyService.getCurrentCompany();
        if (company == null) {
            company = new Company();
        }
        model.addAttribute("company", company);
        return "employers/company_info";
    }

    @PostMapping(value = "/company")
    public RedirectView updateCompany(@ModelAttribute("company") Company company) {
        Company companyFromDb = companyService.getCurrentCompany();
        boolean isCompanyInfoUpdated = companyService.updateCompany(companyFromDb.getId(), company);
        if (!isCompanyInfoUpdated) {
            return new RedirectView("/employers/company");
        }
        return new RedirectView("/employers/home");
    }

    @GetMapping(value = "/account")
    public String account(Model model) {
        User account =  userService.getCurrentUser();
        model.addAttribute("account", account);
        return "employers/internships/internships_list";
    }
}
