package ru.proba.controllers.employers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;
import ru.proba.postgres.models.company.Company;
import ru.proba.postgres.models.company.Supervisor;
import ru.proba.postgres.models.internship.Internship;
import ru.proba.postgres.services.CompanyService;
import ru.proba.postgres.services.SupervisorService;

import java.util.List;

@Controller
@RequestMapping(value = "/employers/supervisors")
public class SupervisorsController {
    @Autowired
    private CompanyService companyService;

    @Autowired
    private SupervisorService supervisorService;

    @GetMapping
    public String supervisorsList(Model model) {
        Company company = companyService.getCurrentCompany();
        if (company != null) {
            model.addAttribute("supervisors", company.getSupervisors());
        } else {
            model.addAttribute("supervisors", null);
        }
        return "employers/supervisors/supervisors_list";
    }

    @GetMapping(value = "/{supervisorId}")
    @PreAuthorize("@supervisorService.findSupervisorById(#supervisorId).company.user.username == @companyService.currentCompany.user.username")
    public String internship(@PathVariable("supervisorId") Long supervisorId, Model model) {
        List<Internship> internships = supervisorService.findInternships(supervisorId);
        Supervisor supervisor = supervisorService.findSupervisorById(supervisorId);
        model.addAttribute("supervisor", supervisor);
        model.addAttribute("internships", internships);
        return "employers/supervisors/supervisor_detailed";
    }

    @GetMapping(value = "/add")
    public String create(Model model) {
        model.addAttribute("supervisor", new Supervisor());
        return "employers/supervisors/supervisor_create";
    }

    @PostMapping(value = "/add")
    public RedirectView add(@ModelAttribute("supervisor") Supervisor supervisor) {
        supervisorService.saveSupervisor(supervisor);
        return new RedirectView("/employers/supervisors");
    }

    @PostMapping("/{supervisorId}/delete")
    @PreAuthorize("@supervisorService.findSupervisorById(#supervisorId).company.user.username == @companyService.currentCompany.user.username")
    public RedirectView delete(@PathVariable("supervisorId") Long supervisorId) {
        supervisorService.deleteSupervisor(supervisorId);
        return new RedirectView("/employers/supervisors");
    }

    @GetMapping(value = "/{supervisorId}/update")
    @PreAuthorize("@supervisorService.findSupervisorById(#supervisorId).company.user.username == @companyService.currentCompany.user.username")
    public String update(Model model, @PathVariable Long supervisorId) {
        Supervisor supervisor = supervisorService.findSupervisorById(supervisorId);
        model.addAttribute("supervisor", supervisor);
        return "employers/supervisors/supervisor_update";
    }

    @PostMapping("/{supervisorId}/update")
    @PreAuthorize("@supervisorService.findSupervisorById(#supervisorId).company.user.username == @companyService.currentCompany.user.username")
    public RedirectView save(@PathVariable Long supervisorId, @ModelAttribute("supervisor") Supervisor supervisor) {
        supervisorService.saveSupervisor(supervisor);
        return new RedirectView("/employers/supervisors");
    }
}
