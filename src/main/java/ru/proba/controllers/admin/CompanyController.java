package ru.proba.controllers.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;
import ru.proba.postgres.models.User;
import ru.proba.postgres.models.company.Company;
import ru.proba.postgres.services.CompanyService;

@Controller
@RequestMapping(value = "/admin/companies")
public class CompanyController {
    @Autowired
    private CompanyService companyService;
    @GetMapping
    public String companies(Model model) {
        model.addAttribute("allCompanies", companyService.allCompanies());
        return "admin/companies/company_list";
    }

    @GetMapping("/{companyId}")
    public String company(@PathVariable("companyId") Long companyId, Model model) {
        model.addAttribute("company", companyService.findCompanyById(companyId));
        return "admin/companies/company_detailed";
    }

    @PostMapping("/{companyId}/delete")
    public RedirectView deleteCompany(@PathVariable("companyId") Long companyId) {
        companyService.deleteCompany(companyId);
        return new RedirectView("/admin/companies");
    }

    @GetMapping("/add")
    public String newCompany(Model model) {
        model.addAttribute("companyForm", new Company());
        model.addAttribute("user", new User());
        return "admin/companies/new_company";
    }

    @PostMapping("/add")
    public RedirectView addCompany(
        @ModelAttribute("companyForm") Company company,
        @ModelAttribute("user") User user
    ) {
        Company newCompany = companyService.addCompany(company, user);
        if (newCompany != null) {
            return new RedirectView("/admin/companies/" + company.getId());
        }
        return new RedirectView("/admin/companies/add");
    }

    @GetMapping("/{companyId}/update")
    public String updateCompany(@PathVariable("companyId") Long companyId, Model model) {
        Company company = companyService.findCompanyById(companyId);
        model.addAttribute("company", company);
        return "admin/companies/update_company";
    }

    @PostMapping("/{companyId}/update")
    public RedirectView updateCompany(@PathVariable("companyId") Long companyId, @ModelAttribute("company") Company company) {
        boolean idCompanyUpdated = companyService.updateCompany(companyId, company);
        if (idCompanyUpdated) {
            return new RedirectView("/admin/companies");
        }
        return new RedirectView("/admin/companies/" + companyId + "/update");
    }
}
