package ru.proba.controllers.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;
import ru.proba.postgres.models.Role;
import ru.proba.postgres.models.User;
import ru.proba.postgres.services.UserService;

@Controller
@RequestMapping(value = "/admin/users")
public class UsersController {
    @Autowired
    private UserService userService;

    @GetMapping
    public String users(Model model) {
        model.addAttribute("allUsers", userService.allUsers());
        return "admin/users/users_list";
    }

    @PostMapping("/{userId}/delete")
    public RedirectView deleteUser(@PathVariable Long userId) {
        userService.deleteUser(userId);
        return new RedirectView("/admin/users");
    }

    @GetMapping("/add")
    public String newUser(Model model) {
        model.addAttribute("user", new User());
        return "admin/users/user_create";
    }

    @PostMapping("/add")
    public RedirectView addUser(
        @ModelAttribute("user") User user
    ) {
        userService.saveUser(user, new Role(3L, "ROLE_USER"));
        return new RedirectView("/admin/users");
    }

    @GetMapping("/add-admin")
    public String newAdmin(Model model) {
        model.addAttribute("user", new User());
        return "admin/users/admin_create";
    }

    @PostMapping("/add-admin")
    public RedirectView addAdmin(
        @ModelAttribute("user") User user
    ) {
        userService.saveUser(user, new Role(1L, "ROLE_ADMIN"));
        return new RedirectView("/admin/users");
    }
}
