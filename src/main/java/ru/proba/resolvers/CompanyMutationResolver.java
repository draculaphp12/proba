package ru.proba.resolvers;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.proba.postgres.models.company.Company;
import ru.proba.postgres.models.internship.Internship;
import ru.proba.postgres.repositories.CompanyJPARepository;
import java.util.List;

@Component
public class CompanyMutationResolver implements GraphQLMutationResolver {
    private final CompanyJPARepository companyRepository;

    public CompanyMutationResolver(CompanyJPARepository companyRepository) {
        this.companyRepository = companyRepository;
    }

    @Transactional
    public Company createCompany(Company company) {
        return companyRepository.save(company);
    }

    @Transactional
    public Company updateCompany(Long id, Company company) {
        Company foundedCompany = companyRepository.findById(id).orElse(null);
        if (foundedCompany == null) {
            return companyRepository.save(company);
        } else {
            foundedCompany.setDescription(company.getDescription());
            foundedCompany.setName(company.getName());
            foundedCompany.setCountWorkers(company.getCountWorkers());
            foundedCompany.setType(company.getType());
            foundedCompany.setSiteUrl(company.getSiteUrl());

            List<Internship> internships = company.getInternships();
            if (internships != null) {
                foundedCompany.setInternships(internships);
            }
        }
        return foundedCompany;
    }

    @Transactional
    public String deleteCompany(Long id) {
        Company company = companyRepository.findById(id).orElse(null);
        if (company != null) {
            companyRepository.delete(company);
            return company.getName();
        }
        return "Not found";
    }
}
