package ru.proba.resolvers;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.proba.postgres.models.User;
import ru.proba.postgres.repositories.UserRepository;

import java.util.List;

@Component
public class UserResolver implements GraphQLQueryResolver {
    @Autowired
    private UserRepository userRepository;

    public List<User> getUsers() {
        return userRepository.findAll();
    }
}
