package ru.proba.resolvers;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.proba.postgres.models.internship.Internship;
import ru.proba.postgres.repositories.internship.InternshipRepository;

import java.util.List;

@Component
public class InternshipResolver implements GraphQLQueryResolver {
    @Autowired
    private InternshipRepository internshipRepository;

    public List<Internship> getInternships() {
        return internshipRepository.findAll();
    }
}
