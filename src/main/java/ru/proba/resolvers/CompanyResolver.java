package ru.proba.resolvers;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import org.springframework.stereotype.Component;
import ru.proba.postgres.models.company.Company;
import ru.proba.postgres.models.internship.Internship;
import ru.proba.postgres.repositories.CompanyJPARepository;
import java.util.List;

@Component
public class CompanyResolver implements GraphQLQueryResolver {

    private final CompanyJPARepository companyJPARepository;

    public CompanyResolver(CompanyJPARepository companyJPARepository) {
        this.companyJPARepository = companyJPARepository;
    }

    public List<Company> getCompanies() {
        return companyJPARepository.findAll();
    }

    public Company getCompany(Long id) {
        return companyJPARepository.findById(id).orElse(null);
    }

    public List<Internship> getCompanyInternships(Long companyId) {
        Company company = companyJPARepository.findById(companyId).orElse(null);
        if (company != null) {
            return company.getInternships();
        }
        return null;
    }
}
