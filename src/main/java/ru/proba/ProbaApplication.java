package ru.proba;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.data.neo4j.repository.config.EnableNeo4jRepositories;

@SpringBootApplication
@EnableNeo4jRepositories("ru.proba.neo4j.repositories")
public class ProbaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProbaApplication.class, args);
	}

}
