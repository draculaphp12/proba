package ru.proba.neo4j.repositories;

import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.neo4j.repository.query.Query;
import ru.proba.neo4j.nodes.Company;

public interface CompanyRepository extends Neo4jRepository<Company, Long> {
    Company findByName(String name);

    @Query("MATCH (c:Company) WHERE c.name = $name SET c.name = $newName RETURN c")
    Company rename(String name, String newName);

    @Query("MATCH (c:Company) WHERE c.name = $name SET c.sector = $updatedSector RETURN c")
    Company updateSector(String name, String updatedSector);

    @Query("MATCH (c:Company) WHERE c.name = $name SET c.countWorkers = $countWorkers RETURN c")
    Company updateCountWorkers(String name, int countWorkers);
}
