package ru.proba.neo4j.nodes;

import org.springframework.data.neo4j.core.schema.GeneratedValue;
import org.springframework.data.neo4j.core.schema.Id;
import org.springframework.data.neo4j.core.schema.Node;

@Node
public class Company {
    @Id
    @GeneratedValue
    private Long id;
    private String name;
    private String sector;
    private int countWorkers;

    private Company() {}

    public Company(String name, String sector, int countWorkers) {
        this.name = name;
        this.sector = sector;
        this.countWorkers = countWorkers;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public int getCountWorkers() {
        return countWorkers;
    }

    public void setCountWorkers(int countWorkers) {
        this.countWorkers = countWorkers;
    }
}
